using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GSdk.Base;
using GSdk.Cortex;

public class GloveTest : MonoBehaviour
{
    private IBoardCentral central = null;
    private IBoardPeripheral peripheral = null;
    bool initialized = false;
    List<float> sensors= new List<float>();
    List<float> handOpenSensors = null;
    List<float> handClosedSensors = null;
    public Texture btnTexture;

    // Use this for initialization
    void Start()
    {
        var boardFactory = new BoardFactory();
        try
        {
            central = boardFactory.MakeBoardCentral();
        }
        catch (System.Exception err)
        {
            System.Console.WriteLine(err.Message);
        }

        central.DiscoverPeripherals();

    }

    // Update is called once per frame
    void Update()
    {
        if (central != null)
        {
            if (central.Peripherals != null)
            {
                if (!initialized && central.Peripherals.Length > 0)
                {

                    foreach (var periph in central.Peripherals)  // this assumes only 1 glove is connected.
                    {                                           // edit to handle multiple gloves
                        peripheral = periph;
                        peripheral.StreamReceived += Peripheral_StreamReceived;
                        peripheral.Start();
                        peripheral.StreamTimeslotsWrite(new StreamTimeslots()
                        {
                            SensorsState = 6
                        });

                    }
                    initialized = true;
                }
            }
        }

    }

    private void OnGUI()
    {
        int y = 350;

        if (GUI.Button(new Rect(10, 10, 350, 50), "Hold Hand flat and press this button"))
            handOpenSensors = sensors;
        if (GUI.Button(new Rect(10, 90, 350, 50), "Make fist, palm down and press this button"))
            handClosedSensors = sensors;

        for (int i = 0; i < sensors.Count; ++i)
        {

            if (handOpenSensors!=null && handClosedSensors!=null)
            {
                if (i == 0) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Thumb Bend  " + norm(i));
                if (i == 2) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Thumb Press " + norm(i));
                if (i == 4) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Index Bend  " + norm(i));
                if (i == 6) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Middle Bend " + norm(i));
                if (i == 8) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Ring Bend   " + norm(i));
                if (i == 10) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Pinky Bend  " + norm(i));
            }
            else
            {
                if (i == 0) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Thumb Bend  " + format(sensors[i]));
                if (i == 2) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Thumb Press " + format(sensors[i]));
                if (i == 4) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Index Bend  " + format(sensors[i]));
                if (i == 6) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Middle Bend " + format(sensors[i]));
                if (i == 8) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Ring Bend   " + format(sensors[i]));
                if (i == 10) GUI.Label(new Rect(10, y + 10 * i, 200, 50), "Pinky Bend  " + format(sensors[i]));
            }
        }
    }

    private string norm(int i)
    {
        return format((sensors[i] - handOpenSensors[i]) / (handClosedSensors[i] - handOpenSensors[i]));
    }

    private string format(float f)
    {
        return f.ToString("0.000");
    }

    private void Peripheral_StreamReceived(object sender, BoardStreamEventArgs e)
    {
        if (e.StreamType == BoardStreamType.SensorsState)
        {
            var args = e as BoardSensorsStateEventArgs;
            sensors = new List<float>();
            for (int i = 0; i < args.Value.Length; ++i) sensors.Add(args.Value[i]);                
            var value = FloatsToString(args.Value);
            Debug.Log("Received sensors state: " + value);
        }
    }

    private static string FloatsToString(float[] value)
    {
        string result = "";
        var index = 0;
        foreach (var element in value)
        {
            if (index != 0)
            {
                result += ", ";
            }
            result += element.ToString();
            index += 1;
        }
        return result;
    }

    public void Stop()
    {
        if (central != null)
        {
            central = null;
        }
        if (peripheral != null)
        {
            if (peripheral.Status != BoardStatus.Disconnected)
            {
                peripheral.Stop();
            }
            peripheral = null;
        }
    }

    private void OnDestroy()
    {
        Stop();
    }
}
