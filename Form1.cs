using System;
using System.ComponentModel;
using System.Windows.Forms;

using GSdkNet.Board;


namespace CaptoGlove
{
    public partial class Form1 : Form
    {
        IBoardPeripheral mainPeripheral = null;
        IBoardManager boardManager = null;

        public Form1()
        {

            InitializeComponent();
            boardManager = BoardFactory.MakeBoardManager();
            boardManager.DiscoverPeripherals();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            foreach (var peripheral in boardManager.Peripherals)  // should be just the one.  
            {                                    
                mainPeripheral = peripheral;       // if more than one glove, change this.
            }
            mainPeripheral.BoardStream += MainPeripheral_BoardStream;
            mainPeripheral.PropertyChanged += MainPeripheral_PropertyChanged;
            mainPeripheral.StreamTimeslotsWriteAsync(new StreamTimeslots() { SensorsState = 6 });
            mainPeripheral.StartAsync();
            
        }

        private void GetSensorDataButton_Click(object sender, EventArgs e)
        {
            mainPeripheral.SensorsStateReadAsync();
        }

        private void MainPeripheral_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var dbg = e;
        }

        private void MainPeripheral_BoardStream(object sender, BoardStreamEventArgs e)
        {
            var dbg = e;
        }


    }
}